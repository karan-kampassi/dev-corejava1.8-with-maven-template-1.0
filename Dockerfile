FROM openjdk:12-alpine
ADD /src/main/resources/application.properties //
ADD /target/corejava1.8-with-maven-template-1.0-SNAPSHOT.one-jar.jar //
ENTRYPOINT ["java", "-jar", "/corejava1.8-with-maven-template-1.0-SNAPSHOT.one-jar.jar"]